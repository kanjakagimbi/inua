import uuid
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from flask_login import LoginManager
from flask_bcrypt import Bcrypt


db = SQLAlchemy()

login_manager = LoginManager()
bcrypt = Bcrypt()


def generate_uuid():
    return str(uuid.uuid4)


@login_manager.user_loader
def load_user(admin_pk):
    return Admin.query.get(str(admin_pk))


class Admin(db.Model, UserMixin):
    __tablename__ = "admin"
    admin_pk = db.Column(db.String(30), primary_key=True, default=generate_uuid)
    username = db.Column(db.String(30), nullable=False, unique=True)
    password = db.Column(db.String(30), nullable=False)


# if Admin.query.count() == 0:
#     admin = Admin(username='kanja', password='adklald')
#     db.session.add(admin)
#     db.session.commit()
