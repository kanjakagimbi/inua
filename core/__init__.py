from flask import Flask
from flask_login import LoginManager
from core.models import db, bcrypt, login_manager
from core.client.routes import clients_views
from flask_bcrypt import Bcrypt


def config_all(app):

    app.config["SECRET_KEY"] = "asdjkakdjkadakdjkakdka"

    bcrypt.init_app(app)
    login_manager.init_app(app)
    config_routes(app)
    config_db(app)

    return app


def config_db(app):
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///inua.db"
    app.config["SQLACHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    with app.app_context():
        db.create_all()


def config_routes(app):
    app.register_blueprint(clients_views)
