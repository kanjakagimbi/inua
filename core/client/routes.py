from flask import Blueprint, render_template, redirect, url_for
from .forms import RegistrationForm, LoginForm
from core.models import Admin, db
from core.models import bcrypt
from flask_login import current_user

clients_views = Blueprint("client", __name__)


@clients_views.route("/admin_register", methods=["GET", "POST"])
def register_admin():
    if current_user.is_authenticated:
        return ""
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
            "utf-8"
        )
        admin = Admin(username=form.username.data, password=hashed_password)
        db.session.add(admin)
        db.session.commit()
        return redirect(url_for("home"))
    # else:
    #     return "<h1> Can't  register"
    return render_template("register.html", form=form)
